$ordinals = []

for i in 2..100
	is_prime = true
	if $ordinals.length < 1 then
		$ordinals << 2
	end
	for prime in $ordinals do
		if i % prime == 0 then
			is_prime = false
		end
	end
	if is_prime then
		$ordinals << i
	end
end

if __FILE__ == $0 then
	for i in 0..$ordinals.size
		puts "#{i} => #{$ordinals[i]}"
	end
end
