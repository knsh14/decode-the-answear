def vigenere_table(n)
  alphabets = ["A".."Z", "a".."z", "0".."9"].map{|r| r.to_a}
  alphabets.map{|ab| [ab, ab.rotate(n)]}.transpose.map{|a| a.join}
end

def encode_vigenere(str,keyword)
  ret = ""
  str.split("").each_with_index{|v,i|
    ret << v.tr(*vigenere_table(keyword[i%keyword.length,1].unpack("U")[0]-65))
  }
  ret
end

def decode_vigenere(str, key)
	ret = ""
	str.split("").each_with_index{|v,i|
		ret << v.tr(*vigenere_table(-1 * key[i%key.length, 1].unpack("U")[0]-65))
	}
	ret
end
