require 'set'
require './utility.rb'

module NumberPlace
		class Solver
			ALL_NUMBERS = Set.new(1..9)
			def initialize(str)
				@grid = str
			end

			#縦をみて候補を探してくる
			def list_candidates_column(x)
				ALL_NUMBERS - (1..9).map{|item| @grid.cell(x, item).to_i}
			end

			#横をみて候補を探してくる
			def list_candidates_row(y)
				ALL_NUMBERS - (1..9).map{|item| @grid.cell(item, y).to_i}
			end

			#グリッドをみて候補を探してくる
			def list_candidates_grid(x, y)
				ALL_NUMBERS - (1..9).map{|item| @grid.cell((item - 1) / 3 + 1 + 3 * ((x - 1)/3), item % 3 + 1 + 3 * ((y - 1)/3)).to_i}
			end

			def list_candidates(x, y)
				list_candidates_column(x) & list_candidates_row(y) & list_candidates_grid(x, y)
			end

			def solve
				if solve_with_backtracking() then
					@grid
				else
					puts "not found result"
				end
			end
			
			def solve_simple
				for x in 1..9 do
					for y in 1..9 do
						if @grid.cell(x,y) == "0" then
							candidate = list_candidates(x, y)
							# 方針１:入る数字が１つしかないコマは確定
							if candidate.size == 1 then
								@grid.set_cell(x, y, candidate.to_a[0])
								next
							end
							# 方針２:各列・行・グリッドにおいてある数字が入りうるコマが１つしかないなら確定
							nc = candidate - (1..9).map{|i| list_candidates(x, i)}.flatten
							if nc.size == 1 then
								@grid.set_cell(x, y, nc.to_a[0])
								next
							end
							nc = candidate - (1..9).map{|i| list_candidates(i, y)}.flatten
							if nc.size == 1 then
								@grid.set_cell(x, y, nc.to_a[0])
								next
							end
							nc = candidate - (1..9).map{|i| list_candidates((i - 1) / 3 + 1 + 3 * ((x - 1)/3), i % 3 + 1 + 3 * ((y - 1)/3))}.flatten
							if nc.size == 1 then
								@grid.set_cell(x, y, nc.to_a[0])
								next
							end
						end
					end
				end
				@grid
			end

			def solve_with_backtracking
				solve_simple()                  # まず、答が確定するところは解き進めます

				next_zero = @grid.index("0")
				return true if next_zero.nil?   # もう0が残っていない＝解答発見

				# 0 のマスに対して、候補を一つずつ仮置きしてみます
				x, y = @grid.index2pos(next_zero)
				list_candidates(x, y).each{|k|
						saved_grid = @grid.clone      # 盤面を保存しておく
						@grid.set_cell(x, y, k)       # 数字を仮置きする
	
						if solve_with_backtracking()
						return true                 # 答が見つかったら盤面を @grid に残したままで終了
						end

						@grid = saved_grid            # 汚れた盤面を保存した状態に戻す
				}
				return false                    # 答が見つからなかった
			end
		end
end
