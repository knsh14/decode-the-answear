require './vigenere.rb'
require './second_page.rb'

grn = /g.{3}n/
mn = /m.{3}s/
for l in ["D","H","N","U","Z"]
	a = decode_vigenere($text, l)
	if grn.match(a) && mn.match(a) then
		puts l
	end
end
