require 'set'
require './utility.rb'

module HoursesTour
		class Solver
			def initialize(str)
				@grid = str
				@path = []
				@solutions = []
			end

			def can_move?(from_x, from_y, to_x, to_y)
				h = Hourse.new(from_x, from_y)
				list = h.get_where_can_go()
				list.include?([to_x, to_y])
			end

			def movable_positions(x, y)
				h = Hourse.new(x, y)
				h.get_where_can_go()
			end

			def solve(start_x, start_y)
				solve_with_backtracking(start_x, start_y, 1)
				@solutions
			end

			def solve_with_backtracking(x, y, k)
				@path << [x, y]
				if k == 9
					@solutions << @path.clone if can_move?(x, y, @path[0][0], @path[0][1])
				else
					movable_positions(x, y).each{|nx, ny|
					if @grid.cell(nx, ny).to_i == k+1
						solve_with_backtracking(nx, ny, k+1)
					end
					}
				end
				@path.pop
			end
		end

		class Hourse
			def initialize(x, y)
				@x = x
				@y = y
			end

			def get_where_can_go
				 hoge = Set.new() + (-8..8).map{|i| if @x + i < 10 && @x + i > 0 && @y + i < 10 && @y + i > 0 then [@x + i, @y + i] else next end}.delete_if {|o| o == nil}
				hoge1 = Set.new() + (-8..8).map{|i| if @x + i < 10 && @x + i > 0 && @y - i < 10 && @y - i > 0 then [@x + i, @y - i] else next end}.delete_if {|o| o == nil}
				can_go = hoge + hoge1 + Set[[@x + 1, @y], [@x - 1, @y], [@x, @y + 1], [@x, @y - 1]] - Set[[@x, @y]]
				can_go
			end
		end
end
